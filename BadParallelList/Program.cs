﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BadParallelList
{
    class Program
    {
        static void Main(string[] args)
        {
            ReaderWriterLockSlim rwls = new ReaderWriterLockSlim();

            Stopwatch sw = new Stopwatch();
            List<int> myInts = new List<int>();

            sw.Start();

            Parallel.For(0, 20000000, (i) =>
            {
                rwls.EnterReadLock();
                try
                {
                   
                }
                finally
                {
                    rwls.ExitReadLock();
                }

                rwls.EnterWriteLock();
                try
                {
                    myInts.Add(i * i);
                }
                finally
                {
                    rwls.ExitWriteLock();
                }
                
            });

            sw.Stop();

            Console.WriteLine("Generated: " + myInts.Count + " squared ints");
            Console.WriteLine("in " + sw.ElapsedMilliseconds + "ms");
            Console.ReadKey();
        }
    }
}
